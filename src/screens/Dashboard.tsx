import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {View,Text} from 'react-native';
import { PrimaryTheme } from '../styles/Theme';
import Home from './Home';
import WishList from './Wishlist';
import OfficalBrand from './OfficialBrands';
import Cart from './Cart';
import Profile from './Profile';
import Icon from '../components/Icon';
import Wishlist from './Wishlist';

interface Props{
    route:any
    navigation:any
  }


export enum ScreenNames {
    HOMESCREEN = 'Home',
    WISHSCREEN= 'WishList',
    OFFSCREEN = 'Offical Brand',
    CARTSCREEN = 'Cart',
    PROFILE= 'Profile',
  }

const HomeStack = createStackNavigator();
const WishlistStack = createStackNavigator();
const OffBrandStack = createStackNavigator();
const CartStack = createStackNavigator();
const ProfileStack = createStackNavigator();

function HomeStackScreen(props:Props) {
    return (
        <HomeStack.Navigator  screenOptions={{ 
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
              },
              headerShown: true, headerTitleAlign: 'center',
              title:'MSHOP',
              headerTitleStyle:{
                  backgroundColor:PrimaryTheme.$LIGHT_COLOR,
                  color:PrimaryTheme.$MATERIAL_COLOR,
                  fontSize:25
              },
              headerLeft:()=>(
                <View style={{flexDirection:'row'}}>
                 <Icon style={{margin:9,}} size={35} name={'menu'} color={PrimaryTheme.$MATERIAL_SECONDARY}/>
              </View>
              ),
              headerRight:()=>(
                <View style={{flexDirection:'row'}}>
                 <Icon style={{margin:9,}} size={25} name={'notifications'} color={PrimaryTheme.$MATERIAL_SECONDARY}/>
              </View>
              )
          }}
        >
          <HomeStack.Screen  name="Home" component={Home} />
        </HomeStack.Navigator>
    );
  }
  function WishStackScreen() {
    return (
        <WishlistStack.Navigator  screenOptions={{ 
          headerShown: false, 
          }}
        >
          <WishlistStack.Screen  name="Home" component={WishList} />
        </WishlistStack.Navigator>
    );
  }
  function OffStackScreen() {
    return (
        <OffBrandStack.Navigator  screenOptions={{ 
          headerShown: false, 
          }}
        >
          <OffBrandStack.Screen  name="Home" component={OfficalBrand} />
        </OffBrandStack.Navigator>
    );
  }
  function CartStackScreen() {
    return (
        <CartStack.Navigator  screenOptions={{ 
          headerShown: false, 
          }}
        >
          <CartStack.Screen  name="Home" component={Cart} />
        </CartStack.Navigator>
    );
  }
  function ProfileStackScreen() {
    return (
        <ProfileStack.Navigator  screenOptions={{ 
          headerShown: false, 
          }}
        >
          <ProfileStack.Screen  name="Home" component={Profile} />
        </ProfileStack.Navigator>
    );
  }
  const Tab = createBottomTabNavigator();
  const TabData =()=>{
      return(
          <>
            <Tab.Navigator 
            tabBarOptions={{
                activeBackgroundColor:PrimaryTheme.$MATERIAL_SECONDARY,
                activeTintColor:PrimaryTheme.$MATERIAL_COLOR,
            }}
              initialRouteName={'Home'}
            >
                <Tab.Screen options={{
                    tabBarIcon:({})=>(
                        <View>
                           <Icon name={'home'}
                                 style={[{color:PrimaryTheme.$MATERIAL_COLOR}]} 
                                 size={25}
                                 />
                        </View>
                    ),
                    
                }} name="Home" component={HomeStackScreen} />
                <Tab.Screen options={{
                    
                    tabBarIcon:({})=>(
                        <View>
                           <Icon name={'bookmarks'}
                                 style={[{color:PrimaryTheme.$MATERIAL_COLOR}]} 
                                 size={25}
                                 />
                        </View>
                    ),
                }} name="WishList" component={WishStackScreen}  />
                <Tab.Screen options={{
                    tabBarIcon:({})=>(
                        <View>
                           <Icon name={'library'}
                                 style={[{color:PrimaryTheme.$MATERIAL_COLOR}]} 
                                 size={25}
                                 />
                        </View>
                    ),
                }} name="Offical Brand" component={OffStackScreen}  />
                <Tab.Screen options={{
                    tabBarIcon:({})=>(
                        <View>
                           <Icon name={'cart'}
                                 style={[{color:PrimaryTheme.$MATERIAL_COLOR}]} 
                                 size={25}
                                 />
                        </View>
                    ),
                    tabBarBadge:"3"
                }} name="Cart" component={CartStackScreen}  />
                <Tab.Screen options={{
                    tabBarIcon:({})=>(
                        <View>
                           <Icon name={'happy'}
                                 style={[{color:PrimaryTheme.$MATERIAL_COLOR}]} 
                                 size={25}
                                 />
                        </View>
                    ),
                    tabBarBadgeStyle:{backgroundColor:'red'}                    
                }} name="Profile" component={ProfileStackScreen}  />
                
            </Tab.Navigator>
          </>
      )
  }
  export default TabData;