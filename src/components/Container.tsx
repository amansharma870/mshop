import * as React from 'react';
import {SafeAreaView, StyleSheet, ViewStyle} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'

interface Props {
  children: any;
  containerStyles?: ViewStyle | ViewStyle[];
}
const Container = (props: Props) => {
  return (
    <SafeAreaView style={[styles.container, props.containerStyles]}>
      {props.children}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent:'center',
    marginHorizontal:wp('5%'),
  },
});

export default Container;