import React from 'react'
import  { NavigationContainer } from '@react-navigation/native'
import {authStack} from './Routes'


export const Navigators =()=>{
    return(
        <NavigationContainer>
            {authStack()}
        </NavigationContainer>
    )
}
